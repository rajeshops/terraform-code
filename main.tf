#EC2 Code for tf
provider "aws" {
#access_key = "${var.aws_accesskey}"
#secret_key = "${var.aws_secretkey}"
region = "us-east-1"
}

resource "aws_instance" "ec2_server" {
ami = "ami-0947d2ba12ee1ff75"
instance_type = "t2.micro"
    tags = {
     Name = "My_EC2_host"
     Environment  = "MyDemo"
    }
}

output "ec2-server-ip-address" {
  value = aws_instance.ec2_server.*.private_ip
}
